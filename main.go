package main

import (
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

func main() {
	branch := getBranch()
	if len(branch) == 0 {
		return
	}
	iUnstage, dUnstage := stats("--shortstat")
	iStage, dStage := stats("--shortstat", "--cached")
	changes := formatChanges(iUnstage, dUnstage, iStage, dStage)
	if len(changes) == 0 {
		changes = "\033[1;32m\u2714"
	}
	fmt.Println("\033[01;35m\u296e", branch, changes)
}

func getBranch() string {
	res, err := exec.Command("git", "branch").Output()
	if err != nil {
		return ""
	}
	branchRe := regexp.MustCompile("(?m)^\\* (.+)")
	if match := branchRe.FindSubmatch(res); match != nil {
		return string(match[1])
	}
	return ""
}

func stats(opts ...string) (string, string) {
	fullCmd := append([]string{"git", "diff"}, opts...)
	output, err := exec.Command(fullCmd[0], fullCmd[1:]...).Output()
	if err != nil {
		return "", ""
	}
	ins := regexp.MustCompile("([0-9]+) ins")
	del := regexp.MustCompile("([0-9]+) del")
	var i, d string
	if res := ins.FindSubmatch(output); res != nil {
		i = string(res[1])
	}
	if res := del.FindSubmatch(output); res != nil {
		d = string(res[1])
	}
	return i, d
}

func formatChanges(iU, dU, iS, dS string) string {
	var formatted []string
	if iU != "" {
		formatted = append(formatted, "\033[1;32m+"+iU)
	}
	if dU != "" {
		formatted = append(formatted, "\033[1;31m-"+dU)
	}
	if iS != "" {
		formatted = append(formatted, "\033[1;32m\u229e "+iS)
	}
	if dS != "" {
		formatted = append(formatted, "\033[1;31m\u229f "+dS)
	}
	return strings.Join(formatted, " ")
}
